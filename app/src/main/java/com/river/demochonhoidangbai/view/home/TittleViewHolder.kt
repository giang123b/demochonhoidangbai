package com.river.demochonhoidangbai.view.home

import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.river.demochonhoidangbai.model.AGroupPost
import com.river.demochonhoidangbai.model.Tittle
import kotlinx.android.synthetic.main.item_tittle.view.*

class TittleViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    fun bindData(tittle: Tittle){
        itemView.textView_tittle_itemTittleScreen.text = tittle.tittle
    }
}