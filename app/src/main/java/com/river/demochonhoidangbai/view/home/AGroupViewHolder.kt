package com.river.demochonhoidangbai.view.home

import android.app.SearchManager
import android.view.View
import androidx.recyclerview.widget.RecyclerView
import com.river.demochonhoidangbai.R
import com.river.demochonhoidangbai.model.AGroupPost
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.item_a_group_post.view.*

class AGroupViewHolder (itemView: View): RecyclerView.ViewHolder(itemView){
    fun bindData(aGroupPost: AGroupPost, itemClickListener: OnItemClickListener){
        itemView.textView_nameOfGroup_itemGroupPostScreen.text = aGroupPost.nameOfGroup
        itemView.textView_amountPost_itemGroupPostScreen.text = aGroupPost.amountOfPost.toString() + " bài viết"

        Picasso.get().load(aGroupPost.avatar).placeholder(R.drawable.avatar)
            .into(
                itemView.imageView_avatarOfGroup_itemGroupPostScreen)

        itemView.setOnClickListener {
            itemClickListener.onItemClicked(aGroupPost)
        }
    }
}