package com.river.demochonhoidangbai.view.home

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.LinearLayoutManager
import com.river.demochonhoidangbai.R
import com.river.demochonhoidangbai.model.AGroupPost
import com.river.demochonhoidangbai.view.profile.ProfileActivity
import com.river.demochonhoidangbai.view_model.ListGroupViewModel
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), OnItemClickListener{

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val listGroupViewModel: ListGroupViewModel = ViewModelProviders.of(this).get(ListGroupViewModel::class.java)

        val aListGroupPostAdapter = AGroupAdapter(this)
        recyclerView_listGroupPost_activityScreen.adapter = aListGroupPostAdapter

        recyclerView_listGroupPost_activityScreen.layoutManager =
            LinearLayoutManager(this, LinearLayoutManager.VERTICAL, false);

        aListGroupPostAdapter.submitList(listGroupViewModel.loadAllData())

        recyclerView_listGroupPost_activityScreen.setOnClickListener {  }

    }

    override fun onItemClicked(aGroupPost: AGroupPost) {
        val intent = Intent(this, ProfileActivity::class.java)
        intent.putExtra("aGroupPost", aGroupPost)
        startActivity(intent)
    }

}
