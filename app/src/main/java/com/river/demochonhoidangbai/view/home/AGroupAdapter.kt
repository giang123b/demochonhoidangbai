package com.river.demochonhoidangbai.view.home

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.river.demochonhoidangbai.R
import com.river.demochonhoidangbai.model.AGroupPost
import com.river.demochonhoidangbai.model.Tittle

class AGroupAdapter(val itemClickListener: OnItemClickListener) : RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    private val TYPE_TITTLE = 1
    private val TYPE_GROUP = 2
    private var mListData = ArrayList<Any>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_TITTLE -> TittleViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_tittle, parent, false)
            )
            else -> AGroupViewHolder(
                LayoutInflater.from(parent.context).inflate(R.layout.item_a_group_post, parent, false)
            )
        }
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        when (mListData[position]) {
            is Tittle -> (holder as TittleViewHolder).bindData(mListData[position] as Tittle)
            else -> (holder as AGroupViewHolder).bindData(mListData[position] as AGroupPost, itemClickListener)
        }
    }

    override fun getItemViewType(position: Int): Int {
        return when (mListData[position]) {
            is Tittle -> TYPE_TITTLE
            else -> TYPE_GROUP
        }
    }

    override fun getItemCount() = mListData.size

    fun submitList(data: ArrayList<Any>) {
        mListData.clear()
        mListData.addAll(data)
        notifyDataSetChanged()
    }
}