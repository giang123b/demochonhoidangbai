package com.river.demochonhoidangbai.view.home

import com.river.demochonhoidangbai.model.AGroupPost

interface OnItemClickListener {
    fun onItemClicked(aGroupPost: AGroupPost)
}