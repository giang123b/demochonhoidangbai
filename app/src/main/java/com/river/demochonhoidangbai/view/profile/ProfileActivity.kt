package com.river.demochonhoidangbai.view.profile

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.google.android.material.tabs.TabLayout
import com.river.demochonhoidangbai.R
import com.river.demochonhoidangbai.model.AGroupPost
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.activity_profile.*
import kotlinx.android.synthetic.main.item_a_group_post.*
import kotlinx.android.synthetic.main.item_a_group_post.view.*

class ProfileActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_profile)

        tabLayout_profileScreen.tabGravity = TabLayout.GRAVITY_FILL
        val adapter = MyAdapter(this, supportFragmentManager,
            tabLayout_profileScreen.tabCount)

        viewPager_profileScreen.adapter = adapter
        viewPager_profileScreen.addOnPageChangeListener(
            TabLayout.TabLayoutOnPageChangeListener(
                tabLayout_profileScreen
            )
        )

        tabLayout_profileScreen.addOnTabSelectedListener(object : TabLayout.OnTabSelectedListener {
            override fun onTabSelected(tab: TabLayout.Tab) {
                viewPager_profileScreen.currentItem = tab.position
            }
            override fun onTabUnselected(tab: TabLayout.Tab) {}
            override fun onTabReselected(tab: TabLayout.Tab) {}
        })

        if (intent != null){
            val aGroupPost = intent.getSerializableExtra("aGroupPost") as? AGroupPost

            Picasso.get().load(aGroupPost?.avatar).placeholder(R.drawable.avatar)
                .into(
                    imageView_avatarUser_profileScreen)

            textView_username_profileScreen.text = aGroupPost?.username
            textView_nameOfGroup_profileScreen.text = aGroupPost?.nameOfGroup
            textView_amountPosts_profileScreen.text = aGroupPost?.amountOfPost.toString()
            textView_amountFollowers_profileScreen.text = aGroupPost?.follows.toString()
            textView_description_profileScreen.text = aGroupPost?.description
        }

    }
}