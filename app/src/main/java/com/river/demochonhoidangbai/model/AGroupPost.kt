package com.river.demochonhoidangbai.model

import java.io.Serializable

class AGroupPost (val id: Int,
                  val avatar: String,
                  val nameOfGroup: String,
                  val amountOfPost: Int,
                  val username: String,
                  val likes: Int,
                  val follows: Int,
                  val description: String,
                  ):Serializable{
}