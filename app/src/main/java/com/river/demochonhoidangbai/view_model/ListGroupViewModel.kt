package com.river.demochonhoidangbai.view_model

import android.content.Intent
import androidx.lifecycle.ViewModel
import com.river.demochonhoidangbai.model.AGroupPost
import com.river.demochonhoidangbai.model.Tittle
import com.river.demochonhoidangbai.view.profile.ProfileActivity

class ListGroupViewModel : ViewModel() {

    fun loadAllData(): ArrayList<Any> {
        val mListGroup = ArrayList<Any>()

        mListGroup.add(Tittle("CÁ NHÂN"))

        mListGroup.add(
            AGroupPost(1,
                "https://static1.bestie.vn/Mlog/ImageContent/201901/screen-shot-2019-01-22-at-124951-pm-a47bd4.png",
                "Đăng lên trang cá nhân",
                300,
                "@TrangCute", 1009, 900, "Thích ngắm trăng, thích đi dạo phố"
            )
        )

        mListGroup.add(Tittle("HỘI ĐÃ THAM GIA"))

        mListGroup.add(
            AGroupPost(
                2,
                "https://i.pinimg.com/236x/3f/c6/07/3fc607bd10827a23185a60253d0e4873.jpg",
                "Yêu xa",
                1000,
                "@CoLenNao",  1009, 900, "Thích ngắm trăng, thích đi dạo phố"
            )
        )

        mListGroup.add(
            AGroupPost(
                3,
                "https://imgix.bustle.com/uploads/image/2018/3/28/17a46e1b-05c4-425a-9c98-8802a5ddac71-girlshylaughingsweetjeanjacket.jpg?w=1020&h=574&fit=crop&crop=faces&auto=format%2Ccompress&cs=srgb&q=70",
                "Buông",
                100,
                "@ToiNhatDinhSePass",  1009, 900, "Thích ngắm trăng, thích đi dạo phố"
            )
        )

        mListGroup.add(
            AGroupPost(
                4,
                "https://photographer.com.vn/wp-content/uploads/2020/08/1597480178_692_Loat-anh-Girl-Xinh-Cap-3-nu-sinh-tuoi-xi-teen.jpg",
                "Chuyên sống ảo",
                98000,
                "@PassRoi", 1009, 900, "Thích ngắm trăng, thích đi dạo phố"
            )
        )

        mListGroup.add(Tittle("HỘI KHÁC"))

        mListGroup.add(
            AGroupPost(
                5,
                "https://1.bp.blogspot.com/-iClsU_7cDrU/XcfYx2KesGI/AAAAAAAATcY/A428ov61qeQAE7ySUhak1KGOWXsOdRTFQCLcBGAsYHQ/s1600/Wap102Com-68-Anh-gai-xinh-mac-ao-dai%2B%25282%2529.jpg",
                "Bún đậu",
                340,
                "@Vuihaha",  1009, 900, "Thích ngắm trăng, thích đi dạo phố"
            )
        )

        mListGroup.add(
            AGroupPost(
                6,
                "https://docbaohomnay.com/wp-content/uploads/2020/10/tai-sao-nhieu-co-gai-xinh-dep-khong-co-ai-theo-duoi-cau-tra-loi-cua-nguoi-dan-ong-co-den-hon-20-nghin-luot-dong-tinh-se-giai-dap-tat-ca-56823.jpg",
                "Mắm tôm",
                1002,
                "@TuyetQua",  1009, 900, "Thích ngắm trăng, thích đi dạo phố"
            )
        )

        for (i in 1..10) {
            mListGroup.add(
                AGroupPost(
                    7,
                    "https://photographer.com.vn/wp-content/uploads/2020/08/1597480178_692_Loat-anh-Girl-Xinh-Cap-3-nu-sinh-tuoi-xi-teen.jpg",
                    "Chuyên sống ảo",
                    98000,
                    "@HelloGuy",  1009, 900, "Thích ngắm trăng, thích đi dạo phố"
                )
            )
        }

        return mListGroup
    }

    fun openProfile(){
//        val intent = Intent(, ProfileActivity::class.java)
    }
}